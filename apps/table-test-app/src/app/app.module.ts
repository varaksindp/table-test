import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { MockTableModule } from '@table-test/mock-table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule, TranslateService } from '@table-test/translate';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    MockTableModule,
    BrowserAnimationsModule,
    TranslateModule.forRoot(),
  ],
  providers: [
    TranslateService
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

import { Component } from '@angular/core';

@Component({
  selector: 'table-test-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'table-test-app';
}

import { BehaviorSubject, combineLatest, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  ColumnsData,
  ColumnsDataObject,
  ColumnsDef,
  FilterData,
  FilterDef,
  FiltersDataObject,
  PageData,
  PageDataObject,
  SortData,
  SortDataObject,
  SortDef,
  ViewDataObject,
  ViewDef,
} from './filter';

export class TableDataStorage {
  constructor(config: TableStorageConfig) {
    this.viewDefaultDef = config.viewDefaultDef || { moduleType: null, type: null };
    this.viewDef = (config.viewDefMapper && config.viewDefMapper(this.viewDefaultDef)) || of(this.viewDefaultDef);
    this.columnsDefaultDef = config.columnsDefaultDef || { columns: [] };
    this.columnsDef = (config.columnsDefMapper && config.columnsDefMapper(this.columnsDefaultDef)) || of(this.columnsDefaultDef);
    this.sortDefaultDef = config.sortDefaultDef || { columns: [] };
    this.sortDef = (config.sortDefMapper && config.sortDefMapper(this.sortDefaultDef)) || of(this.sortDefaultDef);
    this.filtersDefaultDef = config.filtersDefaultDef || [];
    this.filtersDef = (config.filtersDefMapper && config.filtersDefMapper(this.filtersDefaultDef)) || of(this.filtersDefaultDef);

    this.viewDefaultData = config.viewDefaultData || { id: null, name: '' };
    this.columnsDefaultData = config.columnsDefaultData || { columns: [] };
    this.sortDefaultData = config.sortDefaultData || { sort: null };
    this.filtersDefaultData = config.filtersDefaultData || { filters: [] };
    this.pageDefaultData = config.pageDefaultData || { page: { length: 0, pageSize: 1, pageIndex: 0 } };

    this.#viewData = new BehaviorSubject<ViewDataObject>({ ...this.viewDefaultData });
    this.viewData = this.#viewData.asObservable();
    this.#searchData = new BehaviorSubject<FiltersDataObject & SortDataObject & PageDataObject>({
      ...this.filtersDefaultData,
      ...this.sortDefaultData,
      ...this.pageDefaultData,
    });
    this.searchData =
      (config.searchInitData &&
        combineLatest([config.searchInitData, this.#searchData]).pipe(
          map(([x, y]) => ({
            filters: [...x.filters, ...y.filters],
            page: { ...x.page, ...y.page },
            sort: { ...x.sort, ...y.sort },
          })),
        )) ||
      this.#searchData.asObservable();
    this.#columnsData = new BehaviorSubject<ColumnsDataObject>({ ...this.columnsDefaultData });
    this.columnsData =
      (config.columnsInitData &&
        combineLatest([config.columnsInitData, this.#columnsData]).pipe(
          map(([x, y]) => ({
            columns: [...x.columns, ...y.columns],
          })),
        )) ||
      this.#columnsData.asObservable();

    this.loading = new BehaviorSubject<boolean>(false);
    this.notLoading = this.loading.pipe(map((x) => !x));

    this.setState = config.setState || ((state) => {});
    this.getState = config.getState || (() => null);
  }

  readonly viewDefaultDef: ViewDef;
  readonly viewDef: Observable<ViewDef>;
  readonly columnsDefaultDef: ColumnsDef;
  readonly columnsDef: Observable<ColumnsDef>;
  readonly sortDefaultDef: SortDef;
  readonly sortDef: Observable<ColumnsDef>;
  readonly filtersDefaultDef: FilterDef[];
  readonly filtersDef: Observable<FilterDef[]>;

  readonly viewDefaultData: ViewDataObject;
  readonly columnsDefaultData: ColumnsDataObject;
  readonly sortDefaultData: SortDataObject;
  readonly filtersDefaultData: FiltersDataObject;
  readonly pageDefaultData: PageDataObject;

  readonly #viewData: BehaviorSubject<ViewDataObject>;
  readonly viewData: Observable<ViewDataObject>;
  readonly #searchData: BehaviorSubject<SortDataObject & FiltersDataObject & PageDataObject>;
  readonly searchData: Observable<SortDataObject & FiltersDataObject & PageDataObject>;
  readonly #columnsData: BehaviorSubject<ColumnsDataObject>;
  readonly columnsData: Observable<ColumnsDataObject>;

  readonly loading: BehaviorSubject<boolean>;
  readonly notLoading: Observable<boolean>;

  readonly setState?: (state: ViewDataObject & ColumnsDataObject & SortDataObject & FiltersDataObject & PageDataObject) => void;
  readonly getState?: () => ViewDataObject & ColumnsDataObject & SortDataObject & FiltersDataObject & PageDataObject;

  init() {
    const state = this.getState();
    this.#viewData.next({ id: state?.id || this.viewDefaultData.id, name: state?.name || this.viewDefaultData.name });
    this.#columnsData.next({ columns: state?.columns || this.columnsDefaultData.columns });
    this.#searchData.next({
      filters: state?.filters || this.filtersDefaultData.filters,
      sort: state?.sort || this.sortDefaultData.sort,
      page: state?.page || this.pageDefaultData.page,
    });
  }

  setLoading(loading: boolean) {
    this.loading.next(loading);
  }

  columnsChanged(columns: ColumnsData) {
    this.#columnsData.next({ columns });
    this.updateState();
  }

  sortChanged(sort: SortData) {
    this.#searchData.next({ ...this.#searchData.value, sort });
    this.updateState();
  }

  pageChanged(page: PageData) {
    this.#searchData.next({ ...this.#searchData.value, page });
    this.updateState();
  }

  filtersChanged(filters: FilterData[]) {
    this.#searchData.next({ ...this.#searchData.value, filters, page: this.pageDefaultData.page });
    this.updateState();
  }

  viewChanged(view: ViewDataObject & ColumnsDataObject & SortDataObject & FiltersDataObject) {
    this.#viewData.next({ id: view.id, name: view.name });
    this.#columnsData.next({ columns: view.columns });
    this.#searchData.next({ filters: view.filters, sort: view.sort, page: this.pageDefaultData.page });
    this.updateState();
  }

  viewUpdated(view: ViewDataObject) {
    this.#viewData.next({ id: view.id, name: view.name });
    this.updateState();
  }

  useDefaultView() {
    this.#viewData.next({ ...this.viewDefaultData });
    this.#searchData.next({
      ...this.filtersDefaultData,
      ...this.sortDefaultData,
      ...this.pageDefaultData,
    });
    this.#columnsData.next({ ...this.columnsDefaultData });
    this.updateState();
  }

  reload() {
    this.#searchData.next(this.#searchData.value);
  }

  private updateState() {
    this.setState({
      ...this.#viewData.value,
      ...this.#columnsData.value,
      ...this.#searchData.value,
    });
  }
}

export interface TableStorageConfig {
  viewDefaultDef?: ViewDef;
  viewDefMapper?: (def: ViewDef) => Observable<ViewDef>;
  columnsDefaultDef?: ColumnsDef;
  columnsDefMapper?: (def: ColumnsDef) => Observable<ColumnsDef>;
  sortDefaultDef?: SortDef;
  sortDefMapper?: (def: SortDef) => Observable<SortDef>;
  filtersDefaultDef?: FilterDef[];
  filtersDefMapper?: (def: FilterDef[]) => Observable<FilterDef[]>;

  viewDefaultData?: ViewDataObject;
  columnsDefaultData?: ColumnsDataObject;
  sortDefaultData?: SortDataObject;
  filtersDefaultData?: FiltersDataObject;
  pageDefaultData?: PageDataObject;

  searchInitData?: Observable<Partial<SortDataObject & FiltersDataObject & PageDataObject>>;
  columnsInitData?: Observable<Partial<ColumnsDataObject>>;

  setState?: (state: ViewDataObject & ColumnsDataObject & SortDataObject & FiltersDataObject & PageDataObject) => void;
  getState?: () => ViewDataObject & ColumnsDataObject & SortDataObject & FiltersDataObject & PageDataObject;
}

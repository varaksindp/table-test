import { Type } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { combineLatest, Observable, of } from 'rxjs';
import { catchError, map, shareReplay, switchMap, tap } from 'rxjs/operators';
import { CommonPaginationReq } from '../interfaces/common-pagination-req';
import { CommonPaginationRes } from '../interfaces/common-pagination-res';
import { BasicCache, ICache } from '../services/cache.service';

export abstract class Filter {}
export interface FilterX {
  _def: FilterDef;
  _new: boolean;
  _values: { [id: string]: string };
  _close: () => void;
  _remove: () => void;
  _apply: (values: { [id: string]: any }) => void;
}

export function prepareData<T>(
  searchData: Observable<FiltersDataObject & SortDataObject & PageDataObject>,
  setLoading: (loading: boolean) => void,
  resource: (req: CommonPaginationReq<any, any>) => Observable<CommonPaginationRes<T>>,
  filterDef: FilterDef[] = [],
): Observable<Data<T>> {
  return searchData.pipe(
    tap(() => setLoading(true)),
    switchMap((x) => {
      return resource(convertToCommonRequest({ ...x, filters: transformToDeserializeFormat(x, filterDef) })).pipe(
        map((y) => ({ ...y, isError: false })),
        catchError(() => of({ content: [] as T[], totalElements: 0, number: 0, size: 0, isError: true })),
      );
    }),
    map((x) => ({ content: x.content, isError: x.isError, page: { length: x.totalElements, pageSize: x.size, pageIndex: x.number } })),
    tap(() => {
      setLoading(false);
    }),
    shareReplay(1),
  );
}

export interface Data<T> {
  content: T[];
  isError: boolean;
  page: {
    length: number;
    pageSize: number;
    pageIndex: number;
  };
}

export function addActions(columns: Observable<ColumnsDataObject>) {
  return columns.pipe(map((x) => ({ columns: x.columns.concat(['actions']) })));
}

export function mapToKeyLabelKey(array: string[], labelPrefix: string) {
  return array.map((x) => ({ labelKey: `${labelPrefix}${x}`, key: x }));
}

export function removeFilterDefs(defs: FilterDef[], keys: string[]) {
  return defs.filter((y) => !Object.values(y.keys).some((z) => keys.includes(z)));
}

export function defaultPage(pageSize: number) {
  return { length: 0, pageSize, pageIndex: 0 };
}

export function combineObjects<T>(objects: [Observable<T>]): Observable<T>;
export function combineObjects<T, R>(objects: [Observable<T>, Observable<R>]): Observable<T & R>;
export function combineObjects<T, R, S>(objects: [Observable<T>, Observable<R>, Observable<S>]): Observable<T & R & S>;
export function combineObjects<T, R, S, U>(
  objects: [Observable<T>, Observable<R>, Observable<S>, Observable<U>],
): Observable<T & R & S & U>;
export function combineObjects<T, R, S, U, V>(
  objects: [Observable<T>, Observable<R>, Observable<S>, Observable<U>, Observable<V>],
): Observable<T & R & S & U & V>;

export function combineObjects(objects: Observable<any>[]) {
  return combineLatest(objects).pipe(map((x) => x.reduce((acc, current) => ({ ...acc, ...current }), {})));
}

export function convertToCommonRequest(view: FiltersDataObject & SortDataObject & PageDataObject): CommonPaginationReq<any, any> {
  return {
    pagination: { start: 1 + view.page.pageIndex * view.page.pageSize, number: view.page.pageSize },
    search: {
      predicateObject: view.filters.reduce((acc, current) => {
        const obj = current.key.split('.').reduceRight((a: any, o) => ({ [o]: a }), current.value);
        return { ...acc, ...obj };
      }, {}),
    },
    sort: { predicate: view.sort.key, reverse: view.sort.order === Order.DESC },
  };
}

export function transformToDeserializeFormat(x, filterDef: FilterDef[]) {
  return x.filters.flatMap((y) => filterDef.find((z) => z.keys.value === y.key)?.deserialize(y.value) || [{ key: y.key, value: y.value }]);
}

export interface FilterData {
  key: string;
  value: string | number | boolean;
}

export interface FilterDef {
  labelKey: string;
  type: Type<Filter>;
  required: boolean;
  keys: { [id: string]: string };
  options: any;
  render: (values: { [id: string]: any }) => Observable<{ labelKey: string; params: { [id: string]: any } }>;
  deserialize: (value: string) => any;
}

export interface ViewDataObject {
  id: string;
  name: string;
}

export interface ViewDef {
  moduleType: string;
  type: string;
}

export interface FiltersDataObject {
  filters: FilterData[];
}

export interface ColumnsDataObject {
  columns: string[];
}

export interface SortDataObject {
  sort: SortData;
}

export interface PageDataObject {
  page: PageData;
}

export type PageData = PageEvent;

export interface ColumnsDef {
  columns: {
    key: string;
    labelKey: string;
  }[];
}

export type ColumnsData = string[];

export interface SortDef {
  columns: {
    key: string;
    labelKey: string;
  }[];
}

export interface SortData {
  key: string;
  order: Order;
}

export enum Order {
  ASC = 'ASC',
  DESC = 'DESC',
}

export const FILTER_SEPARATOR = ';';

export class BasicTableData extends BasicCache implements ICache {
  getAllData(value: any) {
    return {
      ...(this.behaviorSubject<ViewDataObject>('viewData')?.value || { id: value.id, name: value.name }),
      ...(this.behaviorSubject<FiltersDataObject & SortDataObject & PageDataObject>('searchData')?.value || {
        page: value.page,
        filters: value.filters,
        sort: value.sort,
      }),
      ...(this.behaviorSubject<ColumnsDataObject>('columnsData')?.value || { columns: value.columns }),
    };
  }

  setAllData(value: any) {
    this.setBehaviorSubject('viewData', { id: value.id, name: value.name });
    this.setBehaviorSubject('searchData', { page: value.page, filters: value.filters, sort: value.sort });
    this.setBehaviorSubject('columnsData', { columns: value.columns });
  }

  get viewData() {
    return this.behaviorSubject<ViewDataObject>('viewData');
  }

  setViewData(value: ViewDataObject) {
    return this.setBehaviorSubject('viewData', value);
  }

  get searchData() {
    return this.behaviorSubject<FiltersDataObject & SortDataObject & PageDataObject>('searchData');
  }

  setSearchData(value: FiltersDataObject & SortDataObject & PageDataObject) {
    return this.setBehaviorSubject('searchData', value);
  }

  setSortData(value: SortDataObject) {
    return this.setBehaviorSubject('searchData', {
      ...this.behaviorSubject<FiltersDataObject & SortDataObject & PageDataObject>('searchData').value,
      ...value,
    });
  }

  setPageData(value: PageDataObject) {
    return this.setBehaviorSubject('searchData', {
      ...this.behaviorSubject<FiltersDataObject & SortDataObject & PageDataObject>('searchData').value,
      ...value,
    });
  }

  setFiltersData(value: FiltersDataObject & PageDataObject) {
    return this.setBehaviorSubject('searchData', {
      ...this.behaviorSubject<FiltersDataObject & SortDataObject & PageDataObject>('searchData').value,
      ...value,
    });
  }

  get columnsData() {
    return this.behaviorSubject<ColumnsDataObject>('columnsData');
  }

  setColumnsData(value: ColumnsDataObject) {
    return this.setBehaviorSubject('columnsData', value);
  }

  get loading() {
    return this.behaviorSubject<boolean>('loading');
  }

  setLoading(value: boolean) {
    return this.setBehaviorSubject('loading', value);
  }

  reloadData() {
    return this.setBehaviorSubject('searchData', {
      ...this.behaviorSubject<FiltersDataObject & SortDataObject & PageDataObject>('searchData').value,
    });
  }

  data<T>(
    searchData: Observable<FiltersDataObject & SortDataObject & PageDataObject>,
    resource: (req: CommonPaginationReq<any, any>) => Observable<CommonPaginationRes<T>>,
    filterDef: FilterDef[],
  ) {
    return (
      this.value('data') ||
      this.value(
        'data',
        prepareData(searchData, (loading) => this.setLoading(loading), resource, filterDef),
      )
    );
  }

  getState() {
    return {
      ...this.behaviorSubject<FiltersDataObject & SortDataObject & PageDataObject>('searchData').value,
      ...this.behaviorSubject<ColumnsDataObject>('columnsData').value,
      ...this.behaviorSubject<ViewDataObject>('viewData').value,
    };
  }
}

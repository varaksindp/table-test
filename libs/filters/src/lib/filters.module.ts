import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebFiltersApiService } from './services/web-filters-api.service';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ColumnsSelectComponent } from './components/columns-select/columns-select.component';
import { DaterangeFilterComponent } from './components/daterange-filter/daterange-filter.component';
import { DynamicSelectFilterComponent } from './components/dynamic-select-filter/dynamic-select-filter.component';
import { TranslateModule } from '@table-test/translate';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogComponent } from './components/dialog/dialog.component';
import { MultiselectFilterComponent } from './components/multiselect-filter/multiselect-filter.component';
import { NumberFilterComponent } from './components/number-filter/number-filter.component';
import { NumberRangeFilterComponent } from './components/number-range-filter/number-range-filter.component';
import { OrderSelectComponent } from './components/order-select/order-select.component';
import { TextFilterComponent } from './components/text-filter/text-filter.component';
import { PaginatorComponent } from './components/paginator/paginator.component';
import { SelectFilterComponent } from './components/select-filter/select-filter.component';
import { FiltersComponent } from './components/filters.component';

@NgModule({
  imports: [
    CommonModule,
    MatTableModule,
    MatPaginatorModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    MatChipsModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSelectModule,
    MatDividerModule,
    MatButtonToggleModule,
    MatCheckboxModule,
    MatDialogModule,
    MatListModule,
  ],
  declarations: [
    ColumnsSelectComponent,
    DaterangeFilterComponent,
    MultiselectFilterComponent,
    NumberFilterComponent,
    NumberRangeFilterComponent,
    OrderSelectComponent,
    TextFilterComponent,
    PaginatorComponent,
    SelectFilterComponent,
    DynamicSelectFilterComponent,
    DialogComponent,
    FiltersComponent
  ],
  exports: [
    ColumnsSelectComponent,
    DaterangeFilterComponent,
    MultiselectFilterComponent,
    NumberFilterComponent,
    NumberRangeFilterComponent,
    SelectFilterComponent,
    DynamicSelectFilterComponent,
    OrderSelectComponent,
    TextFilterComponent,
    PaginatorComponent,
    DialogComponent,
    FiltersComponent
  ],
  providers: [WebFiltersApiService]
})
export class FiltersModule {}

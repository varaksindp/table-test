import { Injectable } from '@angular/core';
import { CreateWebFilterReq, UpdateWebFilterReq, WebFilterContent, WebFiltersRes } from '../interfaces/web-filters';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class WebFiltersApiService {
  private url = `http://localhost:4200/`;

  constructor(private http: HttpClient) {}

  createWebFilter(body: CreateWebFilterReq) {
    return this.http.post<WebFilterContent>(this.url, body);
  }

  updateWebFilter(body: UpdateWebFilterReq) {
    return this.http.put<WebFilterContent>(`${this.url}/${body.id}`, body);
  }

  deleteWebFilter(id: string) {
    return this.http.delete(`${this.url}/${id}`);
  }

  getWebFilters(moduleType: string, type: string) {
    return this.http.get<WebFiltersRes>(`${this.url}/${moduleType}/${type}`);
  }
}

import { Location } from '@angular/common';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class StateService {
  constructor(private location: Location) {}

  getStateFilters() {
    return (this.location.getState() as any)?.filters;
  }

  setStateFilters(filters) {
    const state = this.location.getState() as any;
    this.location.replaceState(this.location.path(), '', { ...state, filters: { ...state?.filters, ...filters } });
    return (this.location.getState() as any)?.filters;
  }

  getStateWithDefaults(filters) {
    const state = this.location.getState() as any;
    this.location.replaceState(this.location.path(), '', { ...state, filters: { ...filters, ...state?.filters } });
    return (this.location.getState() as any)?.filters;
  }
}

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class CacheService {
  private data = new Map<string, any>();

  getCache<T extends ICache>(key: string, c: new () => T, initData?: Parameters<T['init']>) {
    let x = this.data.get(key);
    if (!x) {
      const i = new c();
      i.init(initData);
      x = this.data.set(key, i).get(key);
    }
    return x as T;
  }

  eraseCache(key: string) {
    this.data.delete(key);
  }
}

export interface ICache {
  init(...args: any): void;
}

export abstract class BasicCache {
  private _data = new Map<string, any>();

  init(...args: any) {}

  protected behaviorSubject<T>(key: string) {
    return this._data.get(key) as BehaviorSubject<T>;
  }
  protected setBehaviorSubject<T>(key: string, value: T) {
    const x = this._data.get(key);
    return (x ? x.next(value) && x : this._data.set(key, new BehaviorSubject(value)).get(key)) as BehaviorSubject<T>;
  }

  protected value<T>(key: string, data?: T) {
    return (data ? this._data.set(key, data).get(key) : this._data.get(key)) as T;
  }
}

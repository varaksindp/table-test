
import { Order } from '../table/filter';
import { CommonRes } from './common-res';

export type CreateWebFilterReq = BaseWebFilterContent;
export type UpdateWebFilterReq = WebFilterContent;
export type WebFiltersRes = CommonRes<WebFilterContent>;

interface BaseWebFilterContent {
  name: string;
  type: string;
  moduleType: string;
  filters: { key: string; value: string | number | boolean }[];
  columns: string[];
  sort: {
    key: string;
    order: Order;
  };
}

export interface WebFilterContent extends BaseWebFilterContent {
  id: string;
}

export interface CommonPaginationReq<T, E> {
  pagination: {
    start: number;
    number: number;
  };
  sort: {
    predicate: T;
    reverse: boolean;
  };
  search: {
    predicateObject: Partial<E>;
  };
}

import { ChangeDetectionStrategy, Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Order, SortData, SortDef } from '../../table/filter';

@Component({
  selector: 'apw-filters-order-select',
  templateUrl: './order-select.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrderSelectComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) private data: { def: SortDef; close: () => void; apply: (data: SortData) => void; data: SortData },
  ) {}

  orders = [Order.ASC, Order.DESC];
  order: Order;
  key: string;
  def = this.data.def;

  ngOnInit() {
    this.key = this.data.data.key;
    this.order = this.data.data.order;
  }

  applyData() {
    this.data.apply({ key: this.key, order: this.order });
  }

  close() {
    this.data.close();
  }
}

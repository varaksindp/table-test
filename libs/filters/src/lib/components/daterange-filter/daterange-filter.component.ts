import { ChangeDetectionStrategy, Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { of, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DateRangeType } from '../../enums/date-range-type.enum';
import { RelativeDate } from '../../enums/relative-date.enum';
import { Filter, FilterDef, FilterX, FILTER_SEPARATOR } from '../../table/filter';
import { DATE_INPUT_MASK, DATE_ISO_FORMAT_REGEX } from '../../constants/date';

@Component({
  selector: 'apw-filters-daterange-filter',
  templateUrl: './daterange-filter.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DaterangeFilterComponent extends Filter implements OnInit, OnDestroy {
  constructor(private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) private data: FilterX) {
    super();
  }

  form: FormGroup;
  relative: FormControl;
  labelKey: string;
  options: { noTime?: boolean };
  required: boolean;
  typeOptions: { labelKey?: string; key: string }[];
  destroy = new Subject<any>();

  static config(labelKey: string, keys: { value: string }, options: { noTime?: boolean } = {}, required = false): FilterDef {
    return {
      labelKey,
      type: DaterangeFilterComponent,
      keys,
      required,
      options,
      render: (value) => {
        const values = value.value.split(FILTER_SEPARATOR);
        return of(
          values[0] === DateRangeType.ABSOLUTE
            ? { labelKey: 'defaultValueKey', params: { value: `${values[1]} - ${values[2]}` } }
            : values[1] === RelativeDate.LAST_X_DAYS
            ? { labelKey: 'filters.relativeTypes.LAST_N_DAYS', params: { value: values[2] } }
            : { labelKey: `filters.relativeTypes.${values[1]}`, params: null },
        );
      },
      deserialize: (value: string) => {
        const values = value.split(FILTER_SEPARATOR);
        if (values[0] === DateRangeType.ABSOLUTE) {
          return [
            { key: `${keys.value}From`, value: values[1] + (!options.noTime ? 'T00:00:00' : '') },
            { key: `${keys.value}To`, value: values[2] + (!options.noTime ? 'T23:59:59' : '') },
          ];
        } else {
          const { from, to } = this.transformTypeToValues(values);

          return [
            {
              key: `${keys.value}From`,
              value: from.toISOString().split('T')[0] + (!options.noTime ? 'T00:00:00' : ''),
            },
            { key: `${keys.value}To`, value: to.toISOString().split('T')[0] + (!options.noTime ? 'T23:59:59' : '') },
          ];
        }
      },
    };
  }

  private static transformTypeToValues(values) {
    const now = new Date();
    let from;
    let to;
    switch (values[1]) {
      case RelativeDate.TODAY:
        from = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 12);
        to = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 12);
        break;
      case RelativeDate.YESTERDAY:
        from = new Date(now.getFullYear(), now.getMonth(), now.getDate() - 1, 12);
        to = new Date(now.getFullYear(), now.getMonth(), now.getDate() - 1, 12);
        break;
      case RelativeDate.THIS_WEEK:
        from = new Date();
        from.setDate(now.getDate() - (now.getDay() === 0 ? 6 : now.getDay() - 1));
        from.setHours(12);
        to = new Date();
        to.setDate(now.getDate() + (now.getDay() === 0 ? 0 : 7 - now.getDay()));
        to.setHours(12);
        break;
      case RelativeDate.LAST_WEEK:
        from = new Date();
        from.setDate(now.getDate() - (now.getDay() === 0 ? 6 + 7 : now.getDay() + 6));
        from.setHours(12);
        to = new Date();
        to.setDate(now.getDate() - (now.getDay() === 0 ? 7 : now.getDay()));
        to.setHours(12);
        break;
      case RelativeDate.THIS_MONTH:
        from = new Date(now.getFullYear(), now.getMonth(), 1, 12);
        to = new Date(now.getFullYear(), now.getMonth() + 1, 0, 12);
        break;
      case RelativeDate.LAST_MONTH:
        from = new Date(now.getFullYear(), now.getMonth() - 1, 1, 12);
        to = new Date(now.getFullYear(), now.getMonth(), 0, 12);
        break;
      case RelativeDate.THIS_YEAR:
        from = new Date(now.getFullYear(), 0, 1, 12);
        to = new Date(now.getFullYear(), 12, 0, 12);
        break;
      case RelativeDate.LAST_YEAR:
        from = new Date(now.getFullYear() - 1, 0, 1, 12);
        to = new Date(now.getFullYear() - 1, 12, 0, 12);
        break;
      case RelativeDate.LAST_X_DAYS:
        from = new Date(now.getTime() - +values[2] * 24 * 60 * 60 * 1000);
        to = now;
        break;
      default:
        from = now;
        to = now;
        break;
    }
    return { from, to };
  }

  close() {
    this.data._close();
  }

  remove() {
    this.data._remove();
  }

  apply() {
    const val = this.form.value;
    const value =
      (val.asRelative ? DateRangeType.RELATIVE : DateRangeType.ABSOLUTE) +
      FILTER_SEPARATOR +
      (val.asRelative
        ? this.isLastXDaysType(val.relative.type)
          ? val.relative.type + FILTER_SEPARATOR + val.relative.days
          : val.relative.type
        : val.absolute.from + FILTER_SEPARATOR + val.absolute.to);
    this.data._apply({ value });
  }

  ngOnInit() {
    this.labelKey = this.data._def.labelKey;
    this.options = this.data._def.options;
    this.required = this.data._def.required;
    this.typeOptions = Object.values(RelativeDate).map((x) => ({ key: x, labelKey: `filters.relativeTypes.${x}` }));

    this.buildForm((this.data._values.value || FILTER_SEPARATOR).split(FILTER_SEPARATOR));

    const absolute = this.form.get('absolute');
    const relative = this.form.get('relative');
    this.form
      .get('asRelative')
      .valueChanges.pipe(takeUntil(this.destroy))
      .subscribe(() => {
        absolute.get('from').updateValueAndValidity();
        absolute.get('to').updateValueAndValidity();
        relative.get('type').updateValueAndValidity();
        relative.get('days').updateValueAndValidity();
      });
    relative
      .get('type')
      .valueChanges.pipe(takeUntil(this.destroy))
      .subscribe(() => {
        relative.get('days').updateValueAndValidity();
      });
  }

  ngOnDestroy() {
    this.destroy.next(null);
  }

  isLastXDaysType(type) {
    return type === RelativeDate.LAST_X_DAYS;
  }

  private requiredIfValidator(predicate) {
    return (formControl) => {
      if (!formControl.parent) {
        return null;
      }
      if (predicate()) {
        return Validators.required(formControl);
      }
    };
  }

  private buildForm(values) {
    this.form = this.formBuilder.group({
      asRelative: [values[0] === DateRangeType.RELATIVE],
      absolute: this.formBuilder.group({
        from: [
          values[0] === DateRangeType.ABSOLUTE ? values[1] : '',
          [Validators.pattern(DATE_ISO_FORMAT_REGEX), this.requiredIfValidator(() => !this.form.get('asRelative').value)],
        ],
        to: [
          values[0] === DateRangeType.ABSOLUTE ? values[2] : '',
          [Validators.pattern(DATE_ISO_FORMAT_REGEX), this.requiredIfValidator(() => !this.form.get('asRelative').value)],
        ],
      }),
      relative: this.formBuilder.group({
        type: [
          values[0] === DateRangeType.RELATIVE ? values[1] : null,
          [this.requiredIfValidator(() => this.form.get('asRelative').value)],
        ],
        days: [
          values[0] === DateRangeType.RELATIVE && this.isLastXDaysType(values[1]) ? values[2] : null,
          [Validators.min(0), this.requiredIfValidator(() => this.isLastXDaysType(this.form.get('relative').get('type').value))],
        ],
      }),
    });
  }

  get creating() {
    return !!this.data._new;
  }

  get dateMask() {
    return DATE_INPUT_MASK;
  }
}

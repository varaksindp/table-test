import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'apw-ui-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DialogComponent {
  @Input() small = false;
  @Input() wide = false;
}

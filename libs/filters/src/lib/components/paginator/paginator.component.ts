import { Component, EventEmitter, Input, Output } from '@angular/core';
import { PageData } from '../../table/filter';

@Component({
  selector: 'apw-filters-paginator',
  templateUrl: './paginator.component.html',
})
export class PaginatorComponent {
  @Input() page: PageData;
  @Output() pageChanged = new EventEmitter<PageData>();

  get pageData() {
    return this.page || { pageIndex: 0, pageSize: 0, length: 0 };
  }
}

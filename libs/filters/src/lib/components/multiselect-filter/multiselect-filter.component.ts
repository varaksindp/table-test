import { ChangeDetectionStrategy, Component, Inject, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { of } from 'rxjs';
import { Filter, FilterDef, FilterX, FILTER_SEPARATOR } from '../../table/filter';

@Component({
  selector: 'apw-filters-multiselect-filter',
  templateUrl: './multiselect-filter.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MultiselectFilterComponent extends Filter implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) private data: FilterX) {
    super();
  }

  value: FormControl;
  labelKey: string;
  options: { labelKey?: string; key: string }[];
  required: boolean;

  static config(labelKey: string, keys: { value: string }, options: { labelKey?: string; key: string }[], required = false): FilterDef {
    return {
      labelKey,
      type: MultiselectFilterComponent,
      keys,
      required,
      options: {
        options,
      },
      render: (value) => {
        const values = value.value.split(FILTER_SEPARATOR);
        return of({ labelKey: 'defaultValueKey', params: { value: `${values.join(' | ')}` } });
      },
      deserialize: (value: string) => [{ key: keys.value, value: value.split(FILTER_SEPARATOR) }],
    };
  }

  close() {
    this.data._close();
  }

  remove() {
    this.data._remove();
  }

  apply() {
    this.data._apply({ value: this.value.value.join(FILTER_SEPARATOR) });
  }

  ngOnInit() {
    this.labelKey = this.data._def.labelKey;
    this.options = this.data._def.options.options;
    this.required = this.data._def.required;
    const values = (this.data._values.value || '').split(FILTER_SEPARATOR);
    this.value = new FormControl(values, [Validators.required]);
  }

  get creating() {
    return !!this.data._new;
  }
}

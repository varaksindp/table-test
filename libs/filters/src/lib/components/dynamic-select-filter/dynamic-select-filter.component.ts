import { ChangeDetectionStrategy, Component, Inject, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable, of } from 'rxjs';
import { mapTo, shareReplay, startWith } from 'rxjs/operators';
import { Filter, FilterDef, FilterX } from '../../table/filter';

@Component({
  selector: 'apw-filters-dynamic-select-filter',
  templateUrl: './dynamic-select-filter.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DynamicSelectFilterComponent extends Filter implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) private data: FilterX) {
    super();
  }

  value: FormControl;
  labelKey: string;
  options: Observable<{ labelKey?: string; key: string; value: string }[]>;
  required: boolean;
  loading: Observable<boolean>;

  static config(
    labelKey: string,
    keys: { value: string },
    options: Observable<{ labelKey?: string; key: string; value: string }[]>,
    required = false,
  ): FilterDef {
    return {
      labelKey,
      type: DynamicSelectFilterComponent,
      keys,
      required,
      options: {
        options,
      },
      render: (values: { value: string }) => of({ labelKey: 'defaultValueKey', params: { value: values.value } }),
      deserialize: (value: string) => [{ key: keys.value, value: value }],
    };
  }

  close() {
    this.data._close();
  }

  remove() {
    this.data._remove();
  }

  apply() {
    this.data._apply({ value: this.value.value });
  }

  ngOnInit() {
    this.labelKey = this.data._def.labelKey;
    this.options = this.data._def.options.options.pipe(shareReplay(1));
    this.loading = this.options.pipe(mapTo(false), startWith(true));
    this.required = this.data._def.required;
    const values = this.data._values.value || '';
    this.value = new FormControl(values, [Validators.required]);
  }

  get creating() {
    return !!this.data._new;
  }
}

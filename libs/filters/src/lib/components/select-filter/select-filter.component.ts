import { ChangeDetectionStrategy, Component, Inject, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Filter, FilterDef, FilterX } from '../../table/filter';

@Component({
  selector: 'apw-filters-select-filter',
  templateUrl: './select-filter.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectFilterComponent extends Filter implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) private data: FilterX) {
    super();
  }

  value: FormControl;
  labelKey: string;
  options: Observable<{ labelKey?: string; key: string; value: string }[]>;
  required: boolean;

  static config(
    labelKey: string,
    keys: { value: string },
    options: Observable<{ labelKey?: string; key: string; value: string }[]>,
    required = false,
  ): FilterDef {
    return {
      labelKey,
      type: SelectFilterComponent,
      keys,
      required,
      options: {
        options,
      },
      render: (values: { value: string }) =>
        options.pipe(map((x) => ({ labelKey: 'defaultValueKey', params: { value: `${x.find((y) => y.value === values.value)?.key}` } }))),
      deserialize: (value: string) => [{ key: keys.value, value: value }],
    };
  }

  close() {
    this.data._close();
  }

  remove() {
    this.data._remove();
  }

  apply() {
    this.data._apply({ value: this.value.value });
  }

  ngOnInit() {
    this.labelKey = this.data._def.labelKey;
    this.options = this.data._def.options.options;
    this.required = this.data._def.required;
    const values = this.data._values.value || '';
    this.value = new FormControl(values, [Validators.required]);
  }

  get creating() {
    return !!this.data._new;
  }
}

import { ChangeDetectionStrategy, Component, Inject, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { of } from 'rxjs';
import { Filter, FilterDef, FilterX } from '../../table/filter';

@Component({
  selector: 'apw-filters-number-filter',
  templateUrl: './number-filter.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NumberFilterComponent extends Filter implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) private data: FilterX) {
    super();
  }

  value: FormControl;
  labelKey: string;
  required: boolean;
  numberMask: string;

  static config(labelKey: string, keys: { value: string }, required = false, exponent = 0): FilterDef {
    return {
      labelKey,
      type: NumberFilterComponent,
      keys,
      required,
      options: {
        exponent,
      },
      render: (values: { value: string }) => of({ labelKey: 'defaultValueKey', params: { value: `${values.value}` } }),
      deserialize: (value: string) => [{ key: keys.value, value: +value * Math.pow(10, exponent) }],
    };
  }

  close() {
    this.data._close();
  }

  remove() {
    this.data._remove();
  }

  apply() {
    this.data._apply({ value: this.value.value });
  }

  ngOnInit() {
    this.numberMask = `separator${this.data._def.options.exponent ? `.${this.data._def.options.exponent}` : ``}`;
    this.labelKey = this.data._def.labelKey;
    this.required = this.data._def.required;
    this.value = new FormControl(this.data._values.value, [Validators.required]);
  }

  get creating() {
    return !!this.data._new;
  }
}

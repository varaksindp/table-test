import { ChangeDetectionStrategy, Component, Inject, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { of } from 'rxjs';
import { Filter, FilterDef, FilterX, FILTER_SEPARATOR } from '../../table/filter';

@Component({
  selector: 'apw-filters-number-range-filter',
  templateUrl: './number-range-filter.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NumberRangeFilterComponent extends Filter implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) private data: FilterX) {
    super();
  }
  from: FormControl;
  to: FormControl;
  labelKey: string;
  required: boolean;
  numberMask: string;

  static config(labelKey: string, keys: { value: string }, required = false, exponent = 0): FilterDef {
    return {
      labelKey,
      type: NumberRangeFilterComponent,
      keys,
      required,
      options: {
        exponent,
      },
      render: (value) => {
        const values = value.value.split(FILTER_SEPARATOR);
        return of({ labelKey: 'defaultValueKey', params: { value: `${values[0]} - ${values[1]}` } });
      },
      deserialize: (value: string) => [
        { key: `${keys.value}From`, value: +value.split(FILTER_SEPARATOR)[0] * Math.pow(10, exponent) },
        { key: `${keys.value}To`, value: +value.split(FILTER_SEPARATOR)[1] * Math.pow(10, exponent) },
      ],
    };
  }

  close() {
    this.data._close();
  }

  remove() {
    this.data._remove();
  }

  apply() {
    this.data._apply({ value: `${this.from.value}${FILTER_SEPARATOR}${this.to.value}` });
  }

  ngOnInit() {
    this.numberMask = `separator${this.data._def.options.exponent ? `.${this.data._def.options.exponent}` : ``}`;
    this.labelKey = this.data._def.labelKey;
    this.required = this.data._def.required;
    const values = (this.data._values.value || FILTER_SEPARATOR).split(FILTER_SEPARATOR);
    this.from = new FormControl(values[0], [Validators.required]);
    this.to = new FormControl(values[1], [Validators.required]);
  }

  get creating() {
    return !!this.data._new;
  }
}

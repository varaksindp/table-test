
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ChangeDetectionStrategy, Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BehaviorSubject, of } from 'rxjs';
import { catchError, map, shareReplay, switchMap } from 'rxjs/operators';
import { ColumnsData, ColumnsDef } from '../../table/filter';
import { TranslateService } from '@table-test/translate';

@Component({
  selector: 'apw-filters-columns-select',
  templateUrl: './columns-select.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ColumnsSelectComponent implements OnInit {
  constructor(
    private translate: TranslateService,
    @Inject(MAT_DIALOG_DATA) private data: { def: ColumnsDef; close: () => void; apply: (data: ColumnsData) => void; data: ColumnsData },
  ) {}

  list = new BehaviorSubject<{ key: string; labelKey: string; selected: boolean }[]>([]);
  filterByName = new BehaviorSubject('');

  filteredList = this.list.pipe(
    switchMap((x) => this.translate.stream(x.map((y) => y.labelKey)).pipe(map((y) => x.map((z) => ({ ...z, label: y[z.labelKey] }))))),
    shareReplay(1),
    switchMap((x) => {
      return this.filterByName.pipe(map((y) => x.filter((z) => z.label.toLowerCase().includes(y.toLowerCase()))));
    }),
    catchError(() => {
      return of([]);
    }),
  );

  ngOnInit() {
    const selected = this.data.data
      .map((x) => this.data.def.columns.find((y) => y.key === x))
      .map((x) => ({ key: x.key, labelKey: x.labelKey, selected: true }));
    const available = this.data.def.columns
      .filter((x) => !this.data.data.includes(x.key))
      .map((x) => ({ key: x.key, labelKey: x.labelKey, selected: false }));
    this.list.next([...selected, ...available]);
  }

  filter(name: string) {
    this.filterByName.next(name);
  }

  applyData() {
    this.data.apply(this.list.value.filter((x) => x.selected).map((x) => x.key));
  }

  add(list: { key: string; labelKey: string; selected: boolean }[], key: string) {
    this.updateList(list.map((x) => ({ ...x, selected: x.key === key ? true : x.selected })));
  }

  remove(list: { key: string; labelKey: string; selected: boolean }[], key: string) {
    this.updateList(list.map((x) => ({ ...x, selected: x.key === key ? false : x.selected })));
  }

  close() {
    this.data.close();
  }

  private updateList(list: { key: string; labelKey: string; selected: boolean }[]) {
    const selected = list.filter((x) => x.selected);
    const selectedKeys = selected.map((x) => x.key);
    const available = this.data.def.columns
      .filter((x) => !selectedKeys.includes(x.key))
      .map((x) => ({ key: x.key, labelKey: x.labelKey, selected: false }));
    this.list.next([...selected, ...available]);
  }

  drop(list: { key: string; labelKey: string; selected: boolean }[], event: CdkDragDrop<string[]>) {
    moveItemInArray(list, event.previousIndex, event.currentIndex);
    this.updateList(list);
  }

  trackBy(index: number, item: any) {
    return item.key + item.selected ? '1' : '0';
  }
}

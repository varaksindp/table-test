import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { catchError, delay, map, shareReplay, switchMap, takeUntil, tap } from 'rxjs/operators';
import { WebFilterContent } from '../interfaces/web-filters';
import { WebFiltersApiService } from '../services/web-filters-api.service';
import {
  ColumnsData,
  ColumnsDataObject,
  ColumnsDef,
  FilterData,
  FilterDef,
  FiltersDataObject,
  SortData,
  SortDataObject,
  SortDef,
  ViewDataObject,
} from '../table/filter';
import { ColumnsSelectComponent } from './columns-select/columns-select.component';
import { OrderSelectComponent } from './order-select/order-select.component';
import { TranslateService } from '@table-test/translate';

@Component({
  selector: 'apw-filters-filters',
  templateUrl: './filters.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FiltersComponent implements OnChanges, OnInit, OnDestroy {
  renderedOrder: Observable<string>;
  renderedColumns: ColumnsDef;
  renderedView: Observable<string>;

  constructor(
    private api: WebFiltersApiService,
    private dialog: MatDialog,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
  ) {}

  @Input() moduleType: string;
  @Input() type: string;
  @Input() data: ViewDataObject & ColumnsDataObject & SortDataObject & FiltersDataObject;
  @Output() viewUpdated = new EventEmitter<ViewDataObject>();
  @Output() viewChanged = new EventEmitter<ViewDataObject & ColumnsDataObject & SortDataObject & FiltersDataObject>();
  @Output() useDefault = new EventEmitter<void>();
  @Output() reload = new EventEmitter<void>();

  @Input() filtersDef: FilterDef[] = [];
  @Input() filters: FilterData[] = [];
  @Output() filtersChanged = new EventEmitter<FilterData[]>();

  @Input() columnsDef: ColumnsDef;
  @Input() columns: ColumnsData;
  @Output() columnsChanged = new EventEmitter<ColumnsData>();

  @Input() orderDef: SortDef;
  @Input() order: SortData;
  @Output() orderChanged = new EventEmitter<SortData>();

  @Input() openerFilter?: Observable<string>;

  @ViewChild('modalRef') modalRef: any;

  destroy = new Subject<any>();

  activeFilters: { def: FilterDef; values?: any }[];
  availableFilters: { def: FilterDef; values?: any }[];

  open(x: { def: FilterDef; values?: any }, newFilter = false) {
    const dialogRef = this.dialog.open(x.def.type, {
      data: {
        _def: x.def,
        _new: newFilter,
        _values: { ...x.values },
        _close: () => {
          dialogRef.close();
        },
        _remove: () => {
          this.filters = this.filters.filter((y) => !Object.values(x.def.keys).includes(y.key));
          this.update(true);
          dialogRef.close();
        },
        _apply: (values) => {
          this.filters = this.filters.filter((y) => !Object.values(x.def.keys).includes(y.key));
          const a = Object.keys(values).map((key) => ({ key: x.def.keys[key], value: values[key] }));
          this.filters = this.filters.concat(a);
          this.update(true);
          dialogRef.close();
        },
      },
    });
  }

  openColumns() {
    const dialogRef = this.dialog.open(ColumnsSelectComponent, {
      data: {
        def: this.columnsDef,
        data: [...this.columns],
        close: () => {
          dialogRef.close();
        },
        apply: (columns) => {
          this.columns = columns;
          this.updateColumns(true);
          dialogRef.close();
        },
      },
    });
  }

  openOrder() {
    const dialogRef = this.dialog.open(OrderSelectComponent, {
      data: {
        def: this.orderDef,
        data: { ...this.order },
        close: () => {
          dialogRef.close();
        },
        apply: (order) => {
          this.order = order;
          this.updateOrder(true);
          dialogRef.close();
        },
      },
    });
  }

  updateOrder(emit = false) {
    const columnLabelKey = this.orderDef.columns.find((x) => x.key === this.order.key)?.labelKey;
    const sortByLabelKey = 'filters.orderOrder.' + this.order.order;
    this.renderedOrder = this.translate
      .stream([columnLabelKey, sortByLabelKey])
      .pipe(map((x) => `${x[columnLabelKey]} | ${x[sortByLabelKey]}`));
    if (emit) {
      this.orderChanged.emit(this.order);
      this.viewUpdated.emit({ id: null, name: '' });
      this.cdr.markForCheck();
    }
  }

  updateColumns(emit = false) {
    this.renderedColumns = { columns: this.columnsDef.columns.filter((x) => this.columns.includes(x.key)) };
    if (emit) {
      this.columnsChanged.emit(this.columns);
      this.viewUpdated.emit({ id: null, name: '' });
      this.cdr.markForCheck();
    }
  }

  remove(x: { def: FilterDef; values?: any }) {
    this.filters = this.filters.filter((y) => !Object.values(x.def.keys).includes(y.key));
    this.update(true);
  }

  ngOnChanges() {
    if (this.columnsDef) {
      this.updateColumns();
    }
    if (this.orderDef) {
      this.updateOrder();
    }
    this.update();
  }

  ngOnInit() {
    if (this.openerFilter) {
      this.openerFilter
        .pipe(
          takeUntil(this.destroy),
          tap((x) => {
            const availableOpenerFilter = this.availableFilters.find((y) => y.def.keys.value === x);
            if (availableOpenerFilter) {
              this.open(availableOpenerFilter, true);
            }
            const activeOpenerFilter = this.activeFilters.find((y) => y.def.keys.value === x);
            if (activeOpenerFilter) {
              this.open(activeOpenerFilter);
            }
          }),
        )
        .subscribe();
    }
  }

  ngOnDestroy() {
    this.destroy.next(null);
  }

  update(emit = false) {
    const t = this.filtersDef.map((def) => {
      const values = this.objectMap(def.keys, (x) => this.filters.find((y) => y.key === x)?.value);
      if (Object.values(values).filter((x) => x != null).length > 0) {
        return { def, values };
      } else {
        return { def };
      }
    });

    this.activeFilters = t.filter((x) => x.values != null);
    this.availableFilters = t.filter((x) => x.values == null);
    if (emit) {
      this.filtersChanged.emit([...this.filters]);
      this.viewUpdated.emit({ id: null, name: '' });
      this.cdr.markForCheck();
    }
  }

  private objectMap<T>(object: T, mapFn: (value: string) => any) {
    return Object.keys(object).reduce((result, key) => {
      result[key] = mapFn(object[key]);
      return result;
    }, {});
  }

  saveView(template: TemplateRef<any>) {
    const isLoading = new BehaviorSubject<boolean>(true);
    const isError = new BehaviorSubject<boolean>(false);
    const filterByName = new BehaviorSubject<string>('');
    const ref = this.dialog.open(template, {
      data: {
        id: this.data.id,
        name: this.data.name ?? '',
        close: () => {
          ref.close();
        },
        filter: (x) => {
          filterByName.next(x);
        },
        loading: isLoading.asObservable(),
        error: isError.asObservable(),
        list: this.api.getWebFilters(this.moduleType, this.type).pipe(
          map((x) => x.content),
          shareReplay(1),
          tap(() => {
            isLoading.next(false);
          }),
          switchMap((x) => {
            return filterByName.pipe(map((y) => x.filter((z) => z.name.toLowerCase().includes(y.toLowerCase()))));
          }),
          catchError(() => {
            isError.next(true);
            isLoading.next(false);
            return of();
          }),
        ),
        delete: (id: string) => {
          isLoading.next(true);
          isError.next(false);
          this.api
            .deleteWebFilter(id)
            .pipe(
              tap(() => {
                this.viewUpdated.emit({ id: null, name: '' });
                isLoading.next(false);
                ref.close();
              }),
              catchError(() => {
                isError.next(true);
                isLoading.next(false);
                return of();
              }),
            )
            .subscribe();
        },
        save: (name: string, id: string) => {
          isLoading.next(true);
          isError.next(false);
          of({
            name,
            columns: this.data.columns,
            type: this.type,
            moduleType: this.moduleType,
            filters: this.data.filters.map((x) => ({ key: x.key, value: Array.isArray(x.value) ? x.value.join(';') : x.value })),
            sort: this.data.sort,
          })
            .pipe(
              switchMap((req) => {
                if (id) {
                  return this.api.updateWebFilter({ ...req, id });
                }
                return this.api.createWebFilter({ ...req });
              }),
              tap((x) => {
                this.viewUpdated.emit({ id: x.id, name: x.name });
                isLoading.next(false);
                ref.close();
              }),
              catchError(() => {
                isError.next(true);
                isLoading.next(false);
                return of();
              }),
            )
            .subscribe();
        },
      },
    });
  }

  addFilter(template: TemplateRef<any>) {
    const isLoading = new BehaviorSubject<boolean>(true);
    const isError = new BehaviorSubject<boolean>(false);
    const filterByName = new BehaviorSubject<string>('');
    const ref = this.dialog.open(template, {
      data: {
        close: () => {
          ref.close();
        },
        filter: (x) => {
          filterByName.next(x);
        },
        loading: isLoading.asObservable(),
        error: isError.asObservable(),
        list: of(this.availableFilters).pipe(
          delay(0),
          switchMap((x) =>
            this.translate.stream(x.map((y) => y.def.labelKey)).pipe(map((y) => x.map((z) => ({ ...z, label: y[z.def.labelKey] })))),
          ),
          shareReplay(1),
          tap(() => {
            isLoading.next(false);
          }),
          switchMap((x) => {
            return filterByName.pipe(map((y) => x.filter((z) => z.label.toLowerCase().includes(y.toLowerCase()))));
          }),
          catchError(() => {
            isError.next(true);
            isLoading.next(false);
            return of();
          }),
        ),
        select: (filter: { def: FilterDef; values?: any }) => {
          this.open(filter, true);
          ref.close();
        },
      },
    });
  }

  openView(template: TemplateRef<any>) {
    const isLoading = new BehaviorSubject<boolean>(true);
    const isError = new BehaviorSubject<boolean>(false);
    const filterByName = new BehaviorSubject<string>('');
    const ref = this.dialog.open(template, {
      data: {
        close: () => {
          ref.close();
        },
        filter: (x) => {
          filterByName.next(x);
        },
        loading: isLoading.asObservable(),
        error: isError.asObservable(),
        list: this.api.getWebFilters(this.moduleType, this.type).pipe(
          map((x) => x.content),
          shareReplay(1),
          tap(() => {
            isLoading.next(false);
          }),
          switchMap((x) => {
            return filterByName.pipe(map((y) => x.filter((z) => z.name.toLowerCase().includes(y.toLowerCase()))));
          }),
          catchError(() => {
            isError.next(true);
            isLoading.next(false);
            return of();
          }),
        ),
        select: (view: WebFilterContent) => {
          this.viewChanged.emit({ id: view.id, name: view.name, columns: view.columns, filters: view.filters, sort: view.sort });
          ref.close();
        },
      },
    });
  }

  showDefaultView() {
    this.useDefault.emit();
  }

  reloadData() {
    this.reload.emit();
  }
}

export enum DateRangeType {
  RELATIVE = 'REL',
  ABSOLUTE = 'ABS',
}

import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../services/theme.service';

@Component({
  selector: 'table-test-post1',
  templateUrl: './post1.component.html',
  styleUrls: ['../posts.scss'],
})
export class Post1Component implements OnInit {
  constructor(private themeService: ThemeService) {}

  currentThemeClass;

  ngOnInit() {
    this.themeService.currentTheme.subscribe(
      (currentTheme) => (this.currentThemeClass = currentTheme)
    );
  }
}

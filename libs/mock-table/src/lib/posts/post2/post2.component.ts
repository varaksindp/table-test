import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../services/theme.service';

@Component({
  selector: 'table-test-post2',
  templateUrl: './post2.component.html',
  styleUrls: ['../posts.scss'],
})
export class Post2Component implements OnInit {
  constructor(private themeService: ThemeService) {}

  currentThemeClass;

  ngOnInit(): void {
    this.themeService.currentTheme.subscribe(
      (currentTheme) => (this.currentThemeClass = currentTheme)
    );
  }
}

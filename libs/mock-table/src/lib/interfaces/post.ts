export interface Post {
  id: number;
  date: string;
  name: string;
  description: string;
  image: string;
}

export interface PostsById {
  [postId: number]: Post;
}

export interface PostDetail {
  id: string;
  title: string;
  data: string;
}

export interface Paragraph {
  title: string;
  text: string;
  image: string;
}

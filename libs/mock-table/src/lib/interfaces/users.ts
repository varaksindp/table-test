import { StatesEnum } from '../enums/states.enum';
import { CommonPaginationRes } from '../../../../filters/src/lib/interfaces/common-pagination-res';
import { UserColumns } from '../enums/user-columns';
import { CommonPaginationReq } from '../../../../filters/src/lib/interfaces/common-pagination-req';

export interface UsersContent {
  name: string;
  surname: string;
  age: number;
  state: StatesEnum;
}

export type UsersRes = CommonPaginationRes<UsersContent>;
export type UsersReq = CommonPaginationReq<UserColumns, UsersContent>;

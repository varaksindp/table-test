import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { map } from 'rxjs/operators';
import { ThemeService } from '../../services/theme.service';

@Component({
  selector: 'table-test-works-detail',
  templateUrl: './works-detail.component.html',
  styleUrls: ['./works-detail.component.scss'],
})
export class WorksDetailComponent implements OnInit {
  constructor(
    private themeService: ThemeService,
    private route: ActivatedRoute,
  ) {}

  currentThemeClass;
  postId = this.route.params.pipe(map((x) => +x.id));

  ngOnInit() {
    this.themeService.currentTheme.subscribe(
      (currentTheme) => (this.currentThemeClass = currentTheme)
    );
  }
}

import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../services/theme.service';

@Component({
  selector: 'table-test-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.scss']
})
export class PrivacyPolicyComponent implements OnInit {

  constructor(private themeService: ThemeService) { }
  currentThemeClass;

  ngOnInit() {
    this.themeService.currentTheme.subscribe(
      (currentTheme) => (this.currentThemeClass = currentTheme)
    );
  }
}

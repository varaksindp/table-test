import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../services/theme.service';

@Component({
  selector: 'table-test-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
})
export class AboutComponent implements OnInit {
  constructor(private themeService: ThemeService) {}

  currentThemeClass;

  ngOnInit() {
    this.themeService.currentTheme.subscribe(
      (currentTheme) => (this.currentThemeClass = currentTheme)
    );
  }
}

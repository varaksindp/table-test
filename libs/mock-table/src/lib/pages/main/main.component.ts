import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { ThemeService } from '../../services/theme.service';
import { ActivatedRoute, Router } from '@angular/router';
declare var VANTA;

@Component({
  selector: 'table-test-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit {
  constructor(
    private themeService: ThemeService,
    private router: Router,
    private route: ActivatedRoute,
  ) {}

  currentThemeClass;

  ngOnInit() {
    this.themeService.currentTheme.subscribe(currentTheme => this.currentThemeClass = currentTheme);

    VANTA.WAVES({
      el: "#main-header",
      mouseControls: true,
      touchControls: true,
      gyroControls: false,
      minHeight: 210.00,
      minWidth: 200.00,
      scale: 1.00,
      scaleMobile: 1.00
    })
  }

  openGitlab() {
    window.open(
      'https://gitlab.com/varaksindp',
      '_blank'
    );
  }

  openAbout() {
    this.router.navigate(['about'], { relativeTo: this.route });
  }
}

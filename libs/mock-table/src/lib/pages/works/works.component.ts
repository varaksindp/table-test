import { Component, OnDestroy, OnInit } from '@angular/core';
import { ThemeService } from '../../services/theme.service';
import { PostsService } from '../../services/posts.service';
import { Post, PostsById } from '../../interfaces/post';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'table-test-works',
  templateUrl: './works.component.html',
  styleUrls: ['./works.component.scss'],
})
export class WorksComponent implements OnInit, OnDestroy {
  constructor(
    private themeService: ThemeService,
    private postsService: PostsService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  currentThemeClass;
  posts: Post[];
  postsById: PostsById;
  postsSub: Subscription;
  worksLoading = false;

  ngOnInit() {
    this.themeService.currentTheme.subscribe(
      (currentTheme) => (this.currentThemeClass = currentTheme)
    );

    this.postsService
      .getPostsByIdFromLocal()
      .pipe(
        map((posts) => {
          const postsArray: Post[] = [];
          for (const [key, value] of Object.entries(posts)) {
            postsArray.push({ ...value });
          }
          return postsArray;
        })
      )
      .subscribe((posts) => {
        this.posts = posts.sort(
          (firstPostByID, secondPostById) =>
            secondPostById.id - firstPostByID.id
        );
      });
  }

  ngOnDestroy() {
    if (this.postsSub) {
      this.postsSub.unsubscribe();
    }
  }

  toDetail(id: number) {
    this.router.navigate([id], { relativeTo: this.route });
  }
}

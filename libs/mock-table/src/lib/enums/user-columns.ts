export enum UserColumns {
  NAME = 'name',
  SURNAME = 'surname',
  AGE = 'age',
  STATE = 'state',
}

import {
  Component,
  ElementRef,
  HostBinding,
  OnInit, Renderer2,
  ViewChild
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { ThemeService } from './services/theme.service';

@Component({
  selector: 'table-test-lib',
  templateUrl: './table-test-lib.component.html',
  styleUrls: ['./table-test-lib.component.scss'],
})
export class TableTestLibComponent implements OnInit {
  @HostBinding('class') className = '';
  toggleControl = new FormControl(false);

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private themeService: ThemeService
  ) {}

  ngOnInit() {
    // setting Themeing
    this.toggleControl.valueChanges.subscribe((darkMode) => {
      const darkClassName = 'darkMode';
      this.className = darkMode ? darkClassName : '';
      if(darkMode) {
        this.themeService.changeTheme('darkMode');
      } else {
        this.themeService.changeTheme('');
      }
    });
  }

  toPage(pageName: string) {
    switch (pageName) {
      case 'main':
        this.router.navigate([''], { relativeTo: this.route });
        break;
      case 'works':
        this.router.navigate(['works'], { relativeTo: this.route });
        break;
      case 'about':
        this.router.navigate(['about'], { relativeTo: this.route });
        break;
      default:
        break;
    }
  }
}

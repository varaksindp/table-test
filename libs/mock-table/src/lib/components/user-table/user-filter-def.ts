import { FilterDef } from '../../../../../filters/src/lib/table/filter';

export const USER_COLUMNS_LABEL_KEY = 'user.table.columns.';

export const USER_FILTERS_DEF: FilterDef[] = [];

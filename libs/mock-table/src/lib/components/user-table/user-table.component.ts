import { Component, OnInit } from '@angular/core';
import { StateService } from '../../../../../filters/src/lib/services/state.service';
import { UserApiService } from '../../services/user-api.service';
import { TranslateService } from '@table-test/translate';
import { TableDataStorage } from '../../../../../filters/src/lib/table/table';
import {
  addActions,
  mapToKeyLabelKey,
  Order,
  prepareData,
} from '../../../../../filters/src/lib/table/filter';
import { UserColumns } from '../../enums/user-columns';
import { USER_COLUMNS_LABEL_KEY, USER_FILTERS_DEF } from './user-filter-def';

@Component({
  selector: 'table-test-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.scss'],
})
export class UserTableComponent implements OnInit {
  constructor(
    private state: StateService,
    private userApi: UserApiService,
    private translate: TranslateService
  ) {}

  table = new TableDataStorage({
    sortDefaultDef: {
      columns: mapToKeyLabelKey(Object.values(UserColumns), ''),
    },
    sortDefaultData: { sort: { key: UserColumns.NAME, order: Order.ASC } },
    columnsDefaultDef: {
      columns: mapToKeyLabelKey(Object.values(UserColumns), ''),
    },
    columnsDefaultData: {
      columns: [
        UserColumns.NAME,
        UserColumns.SURNAME,
        UserColumns.AGE,
        UserColumns.STATE,
      ],
    },
    filtersDefaultDef: USER_FILTERS_DEF,
    pageDefaultData: { page: { length: 0, pageSize: 100, pageIndex: 0 } },
    setState: (state) => this.state.setStateFilters(state),
    getState: () => this.state.getStateFilters(),
  });

  data = prepareData(
    this.table.searchData,
    (loading) => this.table.loading.next(loading),
    (req) => this.userApi.getUsers(),
    this.table.filtersDefaultDef
  );

  columns = addActions(this.table.columnsData);

  ngOnInit() {
    this.table.init();
  }

  toDetail(element) {}
}

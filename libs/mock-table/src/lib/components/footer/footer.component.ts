import { Component, Input, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { ThemeService } from '../../services/theme.service';

@Component({
  selector: 'table-test-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  constructor(
    private themeService: ThemeService
  ) {}

  currentThemeClass = '';
  @Input() currentTheme: BehaviorSubject<string> = new BehaviorSubject<string>(
    ''
  );

  ngOnInit() {
    this.themeService.currentTheme.subscribe(currentTheme => this.currentThemeClass = currentTheme);
  }

  openFacebook() {
    window.open(
      'https://www.facebook.com/denis.varaksin.90/',
      '_blank'
    );
  }

  openLinkedin() {
    window.open(
      'https://www.linkedin.com/in/denis-varaksin/',
      '_blank'
    );
  }

  openGitlab() {
    window.open(
      'https://gitlab.com/varaksindp',
      '_blank'
    );
  }
}

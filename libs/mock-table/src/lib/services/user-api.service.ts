import { Injectable } from '@angular/core';
import { of } from 'rxjs';

@Injectable()
export class UserApiService {
  getUsers() {
    return of({
      content: [
        { name: 'Denis', surname: 'Varaksin', age: 28, state: 'ACTIVE' },
        { name: 'Vitaly', surname: 'Shyp', age: 26, state: 'ACTIVE' },
      ],
      pageable: {
        sort: {
          sorted: false,
          unsorted: true,
          empty: false,
        },
        offset: 0,
        pageSize: 1,
        pageNumber: 1,
        unpaged: false,
        paged: true,
      },
      last: true,
      totalPages: 1,
      totalElements: 2,
      size: 2,
      number: 2,
      sort: {
        sorted: true,
        unsorted: false,
        empty: false,
      },
      numberOfElements: 1,
      first: true,
      empty: false,
    });
  }
}

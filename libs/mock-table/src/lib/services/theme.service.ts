import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class ThemeService {
  constructor() {
  }
  private currentThemeSource = new BehaviorSubject<string>('');

  currentTheme = this.currentThemeSource.asObservable();

  changeTheme(theme: string) {
    if(theme === 'darkMode') {
      this.currentThemeSource.next('darkMode')
    } else {
      this.currentThemeSource.next('')
    }
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import {Post, PostDetail, PostsById} from '../interfaces/post';

@Injectable()
export class PostsService {
  constructor(private http: HttpClient) {}

  private postsFirebaseUrl = 'https://denis-varaksin-default-rtdb.europe-west1.firebasedatabase.app/';
  private postsLocall = 'assets/posts.json';

  getPostsFromFirebase():Observable<Post[]> {
    return this.http.get(`${this.postsFirebaseUrl}/posts.json`)
      .pipe(map((response: {[key: string]: any}) => {
        return Object.keys(response).map(key => ({
          ...response[key],
          id: key
        }))
      }));
  }

  getPostFromFirebase(id: string) {
    return this.http.get<PostDetail>(`${this.postsFirebaseUrl}/posts-detail/${id}.json`);
  }

  getPostsByIdFromLocal() {
    return this.http.get<PostsById>('assets/data/posts.json');
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './pages/about/about.component';
import { WorksComponent } from './pages/works/works.component';
import { MainComponent } from './pages/main/main.component';
import { PrivacyPolicyComponent } from './pages/privacy-policy/privacy-policy.component';
import { WorksDetailComponent } from './pages/works-detail/works-detail.component';
import {Post1Component} from "./posts/post1/post1.component";
import {Post2Component} from "./posts/post2/post2.component";

const routes: Routes = [
  { path: '', pathMatch: 'full', component: MainComponent },
  {
    path: 'works',
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: WorksComponent,
      },
      // Posts routes
      {
        path: '1',
        component: Post1Component,
      },
      {
        path: '2',
        component: Post2Component,
      }
    ],
  },
  { path: 'about', component: AboutComponent },
  { path: 'privacyPolicy', component: PrivacyPolicyComponent },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class MockTableRoutingModule {}

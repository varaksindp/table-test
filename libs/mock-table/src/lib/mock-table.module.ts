import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableTestLibComponent } from './table-test-lib.component';
import { FiltersModule } from '../../../filters/src/lib/filters.module';
import { UserTableComponent } from './components/user-table/user-table.component';
import { UserApiService } from './services/user-api.service';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCard, MatCardModule } from '@angular/material/card';
import { TranslateModule } from '@table-test/translate';
import { FooterComponent } from './components/footer/footer.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MainComponent } from './pages/main/main.component';
import { WorksComponent } from './pages/works/works.component';
import { AboutComponent } from './pages/about/about.component';
import { MockTableRoutingModule } from './mock-table-routing.module';
import { RouterModule } from '@angular/router';
import { PrivacyPolicyComponent } from './pages/privacy-policy/privacy-policy.component';
import { ThemeService } from './services/theme.service';
import { AngularEmojisModule } from 'angular-emojis';
import { PostsService } from './services/posts.service';
import { WorksDetailComponent } from './pages/works-detail/works-detail.component';
import { KeysPipe } from './pipes/keys.pipe';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { Post1Component } from './posts/post1/post1.component';
import { Post2Component } from './posts/post2/post2.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FiltersModule,
    MatIconModule,
    MatPaginatorModule,
    MatTableModule,
    MatTooltipModule,
    MatButtonModule,
    MatInputModule,
    MatListModule,
    FormsModule,
    ReactiveFormsModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatCheckboxModule,
    MatCardModule,
    TranslateModule.forChild({ resources: [{}, { prefix: 'table-' }] }),
    MatToolbarModule,
    FormsModule,
    MatMenuModule,
    MockTableRoutingModule,
    AngularEmojisModule,
    MatProgressSpinnerModule
  ],
  declarations: [
    TableTestLibComponent,
    UserTableComponent,
    FooterComponent,
    MainComponent,
    WorksComponent,
    AboutComponent,
    PrivacyPolicyComponent,
    WorksDetailComponent,
    KeysPipe,
    Post1Component,
    Post2Component,
  ],
  exports: [TableTestLibComponent],
  providers: [UserApiService, ThemeService, PostsService],
})
export class MockTableModule {}

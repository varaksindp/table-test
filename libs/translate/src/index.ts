export { TranslateResource } from './lib/translate-resource';
export { TranslateModule, TranslateModuleConfig } from './lib/translate.module';
export { TranslateService } from './lib/translate.service';

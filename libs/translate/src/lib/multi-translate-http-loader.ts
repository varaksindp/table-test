import { HttpClient } from '@angular/common/http';
import { TranslateLoader } from '@ngx-translate/core';
import * as merge from 'deepmerge';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError, map, shareReplay } from 'rxjs/operators';
import { TranslateResource } from './translate-resource';

export class MultiTranslateHttpLoader implements TranslateLoader {
  constructor(
    private http: HttpClient,
    private path: string,
    private resources: TranslateResource[],
    private cache: MultiTranslateHttpLoaderCache,
  ) {}

  public getTranslation(lang: string) {
    const requests = (this.resources || [{}]).map((resource) => {
      const path = (this.path || '/assets/i18n/') + (resource.prefix || '') + lang + (resource.suffix || '.json');
      if (this.cache.cache[path] === undefined) {
        this.cache.cache[path] = this.http.get(path).pipe(
          catchError(() => {
            return of({});
          }),
          shareReplay(1),
        );
      }
      return this.cache.cache[path];
    });
    return forkJoin(requests).pipe(map((response) => merge.all(response)));
  }
}

export class MultiTranslateHttpLoaderCache {
  cache: { [id: string]: Observable<object> } = {};
}

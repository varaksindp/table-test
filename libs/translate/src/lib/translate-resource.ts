export interface TranslateResource {
  prefix?: string;
  suffix?: string;
}

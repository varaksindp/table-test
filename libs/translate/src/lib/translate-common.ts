import { BehaviorSubject, Subject } from 'rxjs';
import { filter } from 'rxjs/operators';

export class TranslateCommon {
  private _lang: Subject<string> = new BehaviorSubject(null);

  setLang(lang: string) {
    this._lang.next(lang);
  }

  get lang() {
    return this._lang.pipe(filter((x) => x !== null));
  }
}

import { Injectable } from '@angular/core';
import { TranslateService as NgxTranslateService } from '@ngx-translate/core';
import { BehaviorSubject, merge, of } from 'rxjs';
import { filter, first, map, switchMap, tap } from 'rxjs/operators';
import { TranslateCommon } from './translate-common';

@Injectable()
export class TranslateService {
  loaded = new BehaviorSubject(false);

  constructor(private translateService: NgxTranslateService, private common: TranslateCommon) {
    this.common.lang
      .pipe(
        filter((x) => x !== null),
        tap((x) => this.translateService.use(x)),
      )
      .subscribe();
    this.translateService.onLangChange
      .pipe(
        first(),
        tap(() => {
          this.loaded.next(true);
        }),
      )
      .subscribe();
  }

  init(availableLangs: string[], lang?: string) {
    this.translateService.addLangs(availableLangs);

    const browserLang = this.translateService.getBrowserLang();

    const initLang = [lang, browserLang, availableLangs[0]].find((x) => availableLangs.includes(x));

    return this.setLang(initLang);
  }

  setLang(lang: string) {
    return this.common.setLang(lang);
  }

  get currentLang() {
    return merge(of(this.translateService.currentLang), this.translateService.onLangChange.pipe(map((x) => x.lang)));
  }

  get availableLangs() {
    return this.translateService.getLangs();
  }

  get langs() {
    return this.currentLang.pipe(map((x) => this.availableLangs.map((y) => ({ id: y, active: y === x }))));
  }

  instant(key: string | Array<string>, interpolateParams?: Object): string {
    return this.translateService.instant(key, interpolateParams);
  }

  stream(key: string | string[], interpolateParams?: object) {
    return this.loaded.pipe(
      filter((x) => x),
      switchMap(() => this.translateService.stream(key, interpolateParams)),
    );
  }
}

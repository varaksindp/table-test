import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { InjectionToken, ModuleWithProviders, NgModule } from '@angular/core';
import { TranslateLoader, TranslateModule as NgxTranslateModule } from '@ngx-translate/core';
import { MultiTranslateHttpLoader, MultiTranslateHttpLoaderCache } from './multi-translate-http-loader';
import { TranslateCommon } from './translate-common';
import { TranslateResource } from './translate-resource';
import { TranslateService } from './translate.service';

export const PATH = new InjectionToken<string>('TRANSLATE_PATH');
export const RESOURCES = new InjectionToken<TranslateResource[]>('TRANSLATE_RESOURCES');

export function MultiTranslateHttpLoaderFactory(
  http: HttpClient,
  path: string,
  resources: TranslateResource[],
  cache: MultiTranslateHttpLoaderCache,
) {
  return new MultiTranslateHttpLoader(http, path, resources, cache);
}

export interface TranslateModuleConfig {
  path?: string;
  resources?: TranslateResource[];
}

@NgModule({
  imports: [CommonModule, HttpClientModule],
  exports: [NgxTranslateModule],
})
export class TranslateModule {
  static forRoot(config: TranslateModuleConfig = {}): ModuleWithProviders<TranslateModule> {
    return {
      ngModule: TranslateModule,
      providers: [
        { provide: PATH, useValue: config.path },
        { provide: RESOURCES, useValue: config.resources },
        TranslateCommon,
        MultiTranslateHttpLoaderCache,
        NgxTranslateModule.forRoot({
          isolate: true,
          loader: {
            provide: TranslateLoader,
            useFactory: MultiTranslateHttpLoaderFactory,
            deps: [HttpClient, PATH, RESOURCES, MultiTranslateHttpLoaderCache],
          },
        }).providers,
        TranslateService,
      ],
    };
  }

  static forChild(config: TranslateModuleConfig = {}): ModuleWithProviders<TranslateModule> {
    return {
      ngModule: TranslateModule,
      providers: [
        config.path ? { provide: PATH, useValue: config.path } : [],
        { provide: RESOURCES, useValue: config.resources },
        NgxTranslateModule.forChild({
          isolate: true,
          loader: {
            provide: TranslateLoader,
            useFactory: MultiTranslateHttpLoaderFactory,
            deps: [HttpClient, PATH, RESOURCES, MultiTranslateHttpLoaderCache],
          },
        }).providers,
        TranslateService,
      ],
    };
  }
}
